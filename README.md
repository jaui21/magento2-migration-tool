
Magento Version: 2.2.4

INSTALL MIGRATION TOOL
** check the magento version
php bin/magento --version
..Instal Migration Tool

where <version> must match the version of the Magento 2 codebase.

from repo.magento.com
composer config repositories.magento composer https://repo.magento.com
composer require magento/data-migration-tool:<version>
or 
from github
composer config repositories.data-migration-tool git https://github.com/magento/data-migration-tool
composer require magento/data-migration-tool:<version>

** check the version of the installed tool. The version entry in that file is the version of the Data Migration Tool.
<magento-root>/vendor/magento/data-migration-tool/composer.json



CONFIGURE MIGRATION TOOL
<magento-root>/vendor/magento/data-migration-tool/etc/opensource-to-opensource:
Configuration and scripts for migrating from Magento Open Source 1 to Magento Open Source 2

<magento-root>/vendor/magento/data-migration-tool/etc/opensource-to-opensource/<magento1-version>/

e.g. 
<magento-root>/vendor/magento/data-migration-tool/etc/opensource-to-opensource/1.9.3.4/

** rename config.xml.dist file
cp config.xml.dist config.xml
** edit configuration

<source>
    <database host="127.0.0.1" name="magento1" user="root" password="pass"/>
</source>
<destination>
    <database host="127.0.0.1" name="magento2" user="root" password="pass"/>
</destination>
<options>
    <crypt_key>f3e25abe619dae2387df9fs594f01985</crypt_key>
</options>


**The <crypt_key> tag is mandatory to fill. It can be found in local.xml file which is located in the directory of Magento 1 instance at app/etc/local.xml in <key> tag


RUN MIGRATION TOOL
 
bin/magento migrate:<mode> [-a|--auto] [-r|--reset] {<path to config.xml>}

e.g.
bin/magento migrate:data --auto --reset /var/www/html/promotions/vendor/magento/data-migration-tool/etc/opensource-to-opensource/1.9.3.4/config.xml


where:

<mode> may be: settings, data, or delta

[-a|--auto] is an optional argument that prevents migration from stopping when it encounters integrity check errors.
[-r|--reset] is an optional argument that starts migration from the beginning. You can use this argument for testing migration.

{<path to config.xml>} is the absolute file system path to config.xml; this argument is required.

** SETTINGS mode migrates stores, websites, and system configuration like shipping, payment, tax settings, etc.
Should migrate settings first.
** DATA mode migrate customers, products data etc
** DELTA mode migrate only the changes made in Magento 1 since the last time you migrated data. 

These changes are:
data that customers added via storefront (created orders, reviews, changes in customer profiles, etc.)

all operations with orders in the Magento Admin panel



